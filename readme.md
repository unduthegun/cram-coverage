Small repro to show that cram-tested binaries do not produce coverage files.

Steps to reproduce:
```
  $ opam install . --deps-only
  $ dune build
  $ dune exec ./hello.exe --instrument-with bisect_ppx
  Hello, World!
  $ find . -name '*.coverage' | wc -l
  1
  $ rm *.coverage
  $ dune clean
  $ dune build @run --instrument-with bisect_ppx
  $ find . -name '*.coverage' | wc -l
  1
  $ dune clean
  $ dune runtest --instrument-with bisect_ppx
  $ find . -name '*.coverage' | wc -l
  0
```
